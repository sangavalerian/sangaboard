//Libraries used

#include "StepperF_alt.h"   //Fergus's hacked stepper library
#include <assert.h>
#include <EEPROM.h>
#include <limits.h>


bool mode_serial;

//CONTROL MODE
// Uncomment (exactly) one of the lines below to enable control mode.
//#define SERIAL_MODE
#define GAMEPAD_MODE

#ifdef SERIAL_MODE
  #define mode_serial
#endif

#ifdef GAMEPAD_MODE
  #include "gamePad.h"        //Valerian´s's hacked gamepad library
  #define mode_gamePad

  //Pins configuration
  const int DATA_PIN = 0; 
  const int CLOCK_PIN = 1; 
  const int LATCH_PIN = 2;

  myGame_Pad myGamePad;
#endif

#ifdef ARDUINO_AVR_LEONARDO
  #define SANGABOARDv3
  #define BOARD_STRING "Sangaboard v0.3"
#elif ARDUINO_AVR_SANGABOARD
  #define SANGABOARDv3
  #define BOARD_STRING "Sangaboard v0.3"
#else
  #define SANGABOARDv2
  #define BOARD_STRING "Sangaboard v0.2"
#endif


#define EACH_MOTOR for(int i=0; i<n_motors; i++)
#define VER_STRING "Sangaboard Firmware v1.0"   //was as in the original version by the team 0.5

// The array below has 3 stepper objects, for X,Y,Z respectively
const int n_motors = 3;
long min_step_delay;
const int min_step_delay_eeprom = sizeof(long)*n_motors;
long ramp_time;
const int ramp_time_eeprom = sizeof(long)*(n_motors+1);
const int axis_max_eeprom = sizeof(long)*(n_motors+2);
Stepper* motors[n_motors];
signed long current_pos[n_motors];
long steps_remaining[n_motors];

bool test_mode=false;

// We'll use this input buffer for serial comms
const int INPUT_BUFFER_LENGTH = 64;
char input_buffer[INPUT_BUFFER_LENGTH];

//steps variable
long mysteps = 10; 

int command_prefix(String command, const char ** prefixes, int n_prefixes){
  // Check if the command starts with any of the prefixes in the list.
  // returns the index if so, otherwise returns -1
  for(int i=0; i<n_prefixes; i++){
    if(command.startsWith(prefixes[i])){
      return i;
    }
  }
  return -1;
}


void setup(){
  Serial.begin(115200); //prepare serial for text output 

//  #if defined(mode_serial)
//    delay(100);
//    Serial.print("Control Mode: ");
//    Serial.println("Serial");
//    delay(100);
  //#elif defined(mode_gamePad)
  
  #if defined(mode_gamePad)
    boolean activate_gMode=true;
    delay(1000);
    Serial.print("Control Mode: ");
    Serial.println("gamePad");
    delay(100);
    
    //initialize gamePad pins
    myGamePad.init(DATA_PIN, CLOCK_PIN, LATCH_PIN, activate_gMode);
  #endif


  #if defined(SANGABOARDv2)
    motors[0] = new Stepper(8, 13, 12, 11, 10);
    motors[1] = new Stepper(8, 9, 8, 7, 6);
    motors[2] = new Stepper(8, 5, 4, 3, 2);
  #elif defined(SANGABOARDv3)
    motors[0] = new Stepper(8, 8, 9, 10, 11);
    motors[1] = new Stepper(8, 5, 13, 4, 12);
    motors[2] = new Stepper(8, 6, 7, A5, A4);
  #endif

  EACH_MOTOR{
    motors[i]->setSpeed(1*8.0/4096.0); //using Fergus's speed for now, though this is ignored...
    steps_remaining[i]=0;
    EEPROM.get(sizeof(long)*i, current_pos[i]); //read last saved position from EEPROM
    //current_pos[i] = 0; //alternatively, reset on power cycle!
  }

  EEPROM.get(min_step_delay_eeprom, min_step_delay);
  if(min_step_delay < 0){ // -1 seems to be what we get if it's uninitialised.
    min_step_delay = 1000;
    EEPROM.put(min_step_delay_eeprom, min_step_delay);
  }
  EEPROM.get(ramp_time_eeprom, ramp_time);
  if(ramp_time < 0){ // -1 seems to be what we get if it's uninitialised.
    ramp_time = 0;
    EEPROM.put(ramp_time_eeprom, ramp_time);
  }

  
  Serial.print("Board: ");
  Serial.println(F(BOARD_STRING));
  Serial.print("Firmware: ");
  Serial.println(F(VER_STRING));   //Prints a firmware version.
  Serial.print("\n");
  
}


//#############################################

void stepMove(int ax,int dr){
    long displacement[n_motors];
    EACH_MOTOR displacement[i]=0;
    displacement[ax]= dr*mysteps;
    move_axes(displacement);
    return;
}

void stepMotor(int motor, long dx){
  //make a single step of a single motor.
  current_pos[motor] += dx;
  motors[motor]->stepMotor(((current_pos[motor] % 8) + 8) % 8); //forgive the double-modulo; I need 0-7 even for -ve numbers
}

void releaseMotor(int motor){
  //release the stepper motor (de-enegrise the coils)
  motors[motor]->stepMotor(8);
}

void print_position(){
  EACH_MOTOR{
    if(i>0) Serial.print(" ");
    Serial.print(current_pos[i]);
  }
  Serial.println();
}

//axes movement
int move_axes(long displ[n_motors]){
   long displacement[n_motors];
  // move all the axes in a nice move
  // split displacements into magnitude and direction, and find max. travel
    int ret=0;
    long max_steps = 0;
    long dir[n_motors];
    EACH_MOTOR{
      dir[i] = displ[i] > 0 ? 1 : -1;
      displacement [i] = displ[i]*dir[i];
      if(displacement[i]>max_steps) max_steps=displacement[i];
    }
    // scale the step delays so the move goes in a straight line, with >=1 motor
    // running at max. speed.
    float step_delay[n_motors];
    EACH_MOTOR if(displacement[i]>0){
      step_delay[i] = float(max_steps)/float(displacement[i])*float(min_step_delay);
    }else{
      step_delay[i] = 9999999999;
    }
    // actually make the move
    bool finished = false;
    long distance_moved[n_motors];
    EACH_MOTOR distance_moved[i] = 0;
    unsigned long start = micros();
    float final_scaled_t = (float) max_steps * min_step_delay; //NB total time taken will be final_scaled_t + 2*ramp_time
    
    while(!finished){
      unsigned long now=micros();
      float elapsed_t;
      if(now<start) //overflow in micros() after ~70 min
        elapsed_t=(float) (now+(ULONG_MAX-start));
      else
        elapsed_t = (float) (now-start);

      float scaled_t; //scale time to allow for acceleration
      if(ramp_time > 0){
        // if a ramp time is specified, accelerate at a constant acceleration for the
        // ramp time, then move at constant (maximum) speed, then decelerate.  If the
        // move is shorter than 2*ramp_time, accelerate then decelerate.
        float remaining_t = final_scaled_t + ramp_time - elapsed_t;
        if(elapsed_t < ramp_time && remaining_t > elapsed_t){ //for the first ramp_time, gradually accelerate
          scaled_t = elapsed_t*elapsed_t/(2*ramp_time);
        }else if(remaining_t < ramp_time){
          scaled_t = final_scaled_t - remaining_t*remaining_t/(2*ramp_time);
        }else{
          scaled_t = elapsed_t - ramp_time/2;
        }
      }else{
        scaled_t = elapsed_t;
      }
      finished = true;
      
      EACH_MOTOR{
        if(distance_moved[i] < displacement[i]){
          finished = false; //only if all axes are done are we truly finished.

          // check if it's time to take another step and move if needed.
          if(scaled_t > ((float)distance_moved[i] + 0.5) * step_delay[i]){
            stepMotor(i, dir[i]);
            distance_moved[i]++;
          }
        }
      }
      //delayMicroseconds(2000);
    }
    EEPROM.put(0, current_pos);
    return ret;
}

//#############################################

void loop(){

//*****************************************************************************************************************************
//I am yet to allow both functions, Serail and Game Pad, hence, you have only one option to control the microscope (I guess i 
// need help here). However, serial is enabled in the gamePad mode so as to help debuging.
//*****************************************************************************************************************************
  //Controlling using serial commands
  
 #if defined(mode_serial) //Begin of Serial Commands mode
        // wait for a serial command and read it
      int received_bytes = Serial.readBytesUntil('\n',input_buffer,INPUT_BUFFER_LENGTH-1);
      if(received_bytes > 0){
        input_buffer[received_bytes] = '\0';
        String command = String(input_buffer);
    //    Serial.println("Got command: <"+command+">");
    
        const char* single_axis_moves[3] = {"mrx ", "mry ", "mrz "};
        int axis = command_prefix(command, single_axis_moves, 3);
        if(axis >= 0){
          int preceding_space = command.indexOf(' ',0);
          if(preceding_space <= 0) Serial.println("Bad command.");
          long n_steps = command.substring(preceding_space+1).toInt();
          long displacement[n_motors];
          EACH_MOTOR displacement[i]=0;
          displacement[axis]=n_steps;
          move_axes(displacement);
          Serial.println("done");
          return;
        }
    
        if(command.startsWith("move_rel ") or command.startsWith("mr ")){ //relative move
          int preceding_space = -1;
          long displacement[n_motors];
          EACH_MOTOR{ //read three integers and store in steps_remaining
            preceding_space = command.indexOf(' ',preceding_space+1);
            if(preceding_space<0){
              Serial.println(F("Error: command is mr <int> <int> <int>"));
              break;
            }
            displacement[i] = command.substring(preceding_space+1).toInt();
          }
          move_axes(displacement);
          Serial.println("done.");
          return;
        }
    
        if(command.startsWith("release")){ //release steppers (de-energise coils)
          EACH_MOTOR releaseMotor(i);
          Serial.println("motors released");
          return;
        }
        if(command.startsWith("p?") or command.startsWith("position?")){
          print_position();
          return;
        }
        if(command.startsWith("ramp_time ")){
          int preceding_space = command.indexOf(' ',0);
          if(preceding_space <= 0){
            Serial.println("Bad command.");
            return;
          }
          ramp_time = command.substring(preceding_space+1).toInt();
          EEPROM.put(ramp_time_eeprom, ramp_time);
          Serial.println("done.");
          return;
        }
        if(command.startsWith("ramp_time?")){
          Serial.print("ramp time ");
          Serial.println(ramp_time);
          return;
        }
        if(command.startsWith("min_step_delay ") || command.startsWith("dt ")){
          int preceding_space = command.indexOf(' ',0);
          if(preceding_space <= 0){
            Serial.println("Bad command.");
            return;
          }
          min_step_delay = command.substring(preceding_space+1).toInt();
          EEPROM.put(min_step_delay_eeprom, min_step_delay);
          Serial.println("done.");
          return;
        }
        if(command.startsWith("min_step_delay?") || command.startsWith("dt?")){
          Serial.print("minimum step delay ");
          Serial.println(min_step_delay);
          return;
        }
        if(command.startsWith("zero")){
          EACH_MOTOR current_pos[i]=0;
          Serial.println("position reset to 0 0 0");
          EEPROM.put(0, current_pos);
          return;
        }
    
        if(command.startsWith("version")){
          Serial.println(F(VER_STRING));
          return;
        }
        if(command.startsWith("board")){
          Serial.println(F(BOARD_STRING));
          return;
        }
    
        
        if(command.startsWith("help")){
          Serial.println("");
          Serial.print("Board: ");
          Serial.println(F(BOARD_STRING));
          Serial.print("Firmware: ");
          Serial.println(F(VER_STRING));
          Serial.print("\n");
          
         // #if defined(mode_serial)
          //Serial.println(F("Compiled with Serial Commands support"));

          Serial.println("");
          Serial.println(F("Commands (terminated by a newline character):"));
          Serial.println(F("mrx <d>                        - relative move in x"));
          Serial.println(F("mry <d>                        - relative move in y"));
          Serial.println(F("mrz <d>                        - relative move in z"));
          Serial.println(F("mr <d> <d> <d>                 - relative move in all 3 axes"));
          Serial.println(F("release                        - de-energise all motors"));
          Serial.println(F("p?                             - print position (3 space-separated integers"));
          Serial.println(F("ramp_time <d>                  - set the time taken to accelerate/decelerate in us"));
          Serial.println(F("min_step_delay <d>             - set the minimum time between steps in us."));
          Serial.println(F("dt <d>                         - set the minimum time between steps in us."));
          Serial.println(F("ramp_time?                     - get the time taken to accelerate/decelerate in us"));
          Serial.println(F("min_step_delay?                - get the minimum time between steps in us."));
          Serial.println(F("zero                           - set the current position to zero."));
          Serial.println(F("test_mode <s>                  - set test_mode <on> <off>"));
          Serial.println(F("version                        - get firmware version string"));
          Serial.println("");
          Serial.println("Input Key:");
          Serial.println(F("<d>                            - a decimal integer."));
          Serial.println("");
          Serial.println("--END--");
          
          #if defined(mode_gamePad)
              Serial.println(F("Compiled with SNES Game Pad support"));
              
              Serial.println(F("D-Button                       - X/Y Movements"));
              Serial.println(F("Left Bumper                    - Z -ve movement"));
              Serial.println(F("Right Bumper                   - Z +ve movement"));
              Serial.println(F("SELECT Button                  - get the gain of the light sensor"));
              Serial.println(F("START Button                   - get the allowable gain values of the light sensor"));
              Serial.println(F("X-Button                       - get the integration time in milliseconds"));
              Serial.println(F("Y-Button                       - read the current value from the full spectrum diode"));
              Serial.println(F("A-Button                       - read the current value from the full spectrum diode"));
              Serial.println(F("B-Button                       - read the current value from the full spectrum diode"));
          #endif
          
          return;
        }
        Serial.println(F("Type 'help' for a list of commands."));
      }else{
        delay(1);
        return;
      } //End of serial Commands mode
      
//*****************************************************************************************************************************
//*****************************************************************************************************************************

  // Controlling using gamePad
  // 20 is the time delay for the repeatAfterTics function. (The time delay to activate Buton holding process)
  //****Instructions****
  // A button click: Will process the function as per click. 
  // A button hold:  Will process the function in incrimental, after a defined time (in this case, i have used 20 secs, Feel free to change tothe value that suits you).
  
  #elif defined(mode_gamePad) //Begin of gamePad mode
      myGamePad.poll(); //read all controllers at once
    
      if(myGamePad.pressed(myGame_Pad::START, 20)){  //Not used
        Serial.println("Start Button");
        return;
      }
      if(myGamePad.pressed(myGame_Pad::SELECT, 20)){ //Not used
        Serial.println("Select Button");
        return;
      }
      if(myGamePad.pressed(myGame_Pad::X, 20)){  //Not used
        Serial.println("X Button");
        return;
      }
      if(myGamePad.pressed(myGame_Pad::Y, 20)){ //Not used
        Serial.println("Y Button");
        return;
      }
      if(myGamePad.pressed(myGame_Pad::A, 20)){ //Increasing the step size per button click (Hold is allowed)
        mysteps=mysteps+10;
        Serial.println("Increase step Size.");
        Serial.print("Steps: ");
        Serial.println(mysteps);
        return;
      }
      if(myGamePad.pressed(myGame_Pad::B, 20)){  //Decreasing the step size per button click (Hold is allowed)
        if (mysteps!=0){
          mysteps=mysteps-10;
        }
        Serial.println("Decrease step Size.");
        Serial.print("Steps: ");
        Serial.println(mysteps);
        return;
      }
      if(myGamePad.pressed(myGame_Pad::UP, 20)){  // Y-Axis goes away from you (Hold is allowed)
        Serial.println("UP Button");
        int axis = 1;
        int dir = -1;
        stepMove(axis,dir);
        return;
      }
      if(myGamePad.pressed(myGame_Pad::DOWN, 20)){  // Y-Axis goes towards you (Hold is allowed)
        Serial.println("DOWN Button");
        int axis = 1;
        int dir = +1;
        stepMove(axis,dir);
        return;
      }
      if(myGamePad.pressed(myGame_Pad::LEFT, 20)){  // X-Axis goes left (anti Clockwise, Hold is allowed)
        Serial.println("LEFT Button");
        int axis = 0;
        int dir = -1;
        stepMove(axis,dir);
        return;
      }
      if(myGamePad.pressed(myGame_Pad::RIGHT, 20)){  // X-Axis goes right (Clockwise, Hold is allowed)
        Serial.println("RIGHT Button");
        int axis = 0;
        int dir = +1;
        stepMove(axis,dir);
        return;
      }
      if(myGamePad.pressed(myGame_Pad::L, 20)){  // Z-Axis DOWN (Hold is allowed)
        Serial.println("L Button");
        int axis = 2;
        int dir = -1;
        stepMove(axis,dir);
        return;
      }
       if(myGamePad.pressed(myGame_Pad::R, 20)){  // Z-Axis UP (Hold is allowed)
        Serial.println("R Button");
        int axis = 2;
        int dir = +1;
        stepMove(axis,dir);
        return;
      } //End of gamePad mode
      delay(50);
      
  #endif //End of selection modes
  
}
