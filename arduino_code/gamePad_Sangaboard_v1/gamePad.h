
#pragma once

//Begining of the gamePad Class
class myGame_Pad{
  
  public:

  enum Button{
    B = 0,
    Y = 1,
    SELECT = 2,
    START = 3,
    UP = 4,
    DOWN = 5,
    LEFT = 6,
    RIGHT = 7,
    A = 8,
    X = 9,
    L = 10,
    R = 11,
  };

  int latchPin;
  int clockPin;
  int buttons[12];
  int dataPin;

  boolean gamePad_mode=false;

  //The function that initiates gamepad
  void init(int data, int clock, int latch, bool isPad){
    
    gamePad_mode= isPad;
    dataPin = data;
    clockPin = clock;
    latchPin = latch;
    
    pinMode(dataPin, INPUT_PULLUP);
    pinMode(latchPin, OUTPUT);
    digitalWrite(latchPin, LOW);
    pinMode(clockPin, OUTPUT);
    
    digitalWrite(clockPin, HIGH);
  }
  
  void poll(){
    digitalWrite(latchPin, HIGH);
    delayMicroseconds(12);
    digitalWrite(latchPin, LOW);  
    delayMicroseconds (6);
    
    for(int i = 0; i < 12; i++){
        if(dataPin > -1){
          if(digitalRead(dataPin))
            buttons[i] = -1;
          else
            buttons[i]++;
        }
        
      digitalWrite(clockPin, LOW);
      delayMicroseconds(6);
      digitalWrite(clockPin, HIGH);
      delayMicroseconds(6);
    }
  }

  int translate(Button b) const{
    if(gamePad_mode) return b;
  }

  ///button will be unpressed until released again
  void clear(Button b){
    buttons[translate(b)] = 0x80000000;
  }

  ///returns if button is currently down
  bool down(Button b) const{
    return buttons[translate(b)] >= 0;
  }

   //returns true if button state changed to down since previous poll.
   //repeatAfterTics can be used to repeat after button was hold down for some time.
  bool pressed(Button b, int repeatAfterTics = 0x7fffffff) const{
    return buttons[translate(b)] == 0 || (buttons[translate(b)] >= repeatAfterTics);
  }
  
};
//END OF CLASS
